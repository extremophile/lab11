#include <iostream>

using namespace std;

void count_all(int number, int &odd, int &even)
{
odd = 0, even = 0;
while(number)
{
int lastDigit = number % 10;
if(lastDigit % 2)
{
odd++;
}
else
{
even++;
}
number /= 10;
}
}


int main()
{
int n;
cout << "Enter how many numbers will be read: " << endl;
cin >> n;

while(n > 0)
{
cout << "Enter a number: " << endl;
int number;
cin >> number;

int odd, even;
count_all(number, odd, even);
cout <<odd << " " << even << endl;

}



}
